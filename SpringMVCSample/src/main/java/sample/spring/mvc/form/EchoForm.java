package sample.spring.mvc.form;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;

@Data
public class EchoForm {

    @NotNull
    @Size(min = 1, max = 5)
    private String name;
}
