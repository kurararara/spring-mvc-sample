package sample.spring.mvc.service;

import java.util.Optional;

import org.springframework.stereotype.Service;

import sample.spring.mvc.dto.CardDto;

@Service
public class UseSQLService {

    public Optional<CardDto> searchCardById() {

        // TODO: 検索処理

        //@formatter:off
        return Optional.of(CardDto.builder()
                .name("くらもとやすひろ")
                .id("@kurararara")
                .cont("超よわよわエンジニア")
                .build());
        //@formatter:on
    }
}
