package sample.spring.mvc.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sample.spring.mvc.dto.CardDto;

@Repository
public interface CardRepository extends JpaRepository<CardDto, String> {

}
