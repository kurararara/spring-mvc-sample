package sample.spring.mvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import sample.spring.mvc.form.UseSQLForm;
import sample.spring.mvc.service.UseSQLService;

@Controller
@RequestMapping("use-sql")
public class UseSQLController {

    @Autowired
    UseSQLService service;

    @ModelAttribute
    public UseSQLForm setUpUseSQLForm() {
        UseSQLForm form = new UseSQLForm();
        return form;
    }

    @RequestMapping
    public String index(Model model) {

        //@formatter:off
        service.searchCardById().ifPresent(card -> {
            model.addAttribute("name", card.getName());
            model.addAttribute("id", card.getId());
            model.addAttribute("cont", card.getCont());
        });
        //@formatter:on

        return "use-sql/select";
    }

}
