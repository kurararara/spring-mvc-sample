package sample.spring.mvc.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CardDto {

    /** 名前 */
    private String name;

    /** id */
    private String id;

    /** 内容 */
    private String cont;

}
