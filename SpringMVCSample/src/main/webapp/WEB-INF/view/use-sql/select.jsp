<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<title>SQL Sample Application</title>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.css">
</head>
<body>
<section class="section">
  <div class="container">
    <div class="card">
      <div class="card-content">
        <div class="media">
         <div class="media-left">
            <figure class="image is-48x48">
              <img src="https://bulma.io/images/placeholders/96x96.png" alt="Placeholder image">
            </figure>
          </div>
          <div class="media-content">
            <p class="title is-4"><c:out value="${name}" /></p>
            <p class="subtitle is-6"><c:out value="${id}" /></p>
          </div>
        </div>

        <div class="content">
          <c:out value="${cont}" />
        </div>
      </div>
    </div>
  </div>
</section>
</body>
</html>